import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { GalleryPage } from '../pages/gallery/gallery';
import { ChatListPage } from '../pages/chat-list/chat-list';

import { DatabaseProvider } from '../providers/database/database'

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor (
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public dbProvider: DatabaseProvider,
    public storage: Storage
  ){

    this.initializeApp();

    storage.set("station-list", [{"lat":-15.8747447,"long":-48.1051392,"title":"O MELHOR","priceIncrease":true,"priceMoney":3.99,"priceChange":0.14,"image":"o-melhor.jpg","address":"Samambaia - QD 409"},{"lat":-15.8698407,"long":-48.0931245,"title":"CAPITAL","priceIncrease":false,"priceMoney":4.19,"priceChange":0.19,"image":"ale.jpg","address":"Samambaia - QD 401"},{"lat":-15.872682,"long":-48.0948679,"title":"IPIRANGA","priceIncrease":false,"priceMoney":4.09,"priceChange":0.19,"image":"ipiranga.jpg","address":"Samambaia - QD 203"},{"lat":-15.8680012,"long":-48.0681396,"title":"O MELHOR","priceIncrease":true,"priceMoney":3.89,"priceChange":0.09,"image":"o-melhor.jpg","address":"Samambaia - QD 114"},{"lat":-15.857563,"long":-48.0776412,"title":"PETROBRAS","priceIncrease":true,"priceMoney":3.99,"priceChange":0.19,"image":"br.jpg","address":"Samambaia - QD 410"}]);

    this.pages = [
      { title: 'Stations',component: HomePage },
      { title: 'Map',     component: MapPage },
      { title: 'Chat',    component: ChatListPage },
      { title: 'Gallery', component: GalleryPage }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      let that: any = this;
      //Create DB
      this.dbProvider.createDatabase()
        .then(() => {
          // closing SplashScreen only when the DB by created
          that.splashScreen.hide();
        })
        .catch(() => {
          // or if have error in create DB
          that.splashScreen.hide();
        });
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
