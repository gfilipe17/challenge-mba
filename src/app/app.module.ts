import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { SQLite } from '@ionic-native/sqlite'

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { GalleryPage, NavigationDetailsPage } from '../pages/gallery/gallery';
import { ChatListPage } from '../pages/chat-list/chat-list';
import { ChatPage } from '../pages/chat/chat';

import { EmojiProvider } from '../providers/emoji';
import { ChatService } from '../providers/chat-service';
import { DatabaseProvider } from '../providers/database/database';

import { EmojiPickerComponent } from '../components/emoji-picker/emoji-picker';
import { RelativeTime } from '../pipes/relative-time';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { Geolocation } from '@ionic-native/geolocation';
import { BLE } from '@ionic-native/ble';
import { Toast } from '@ionic-native/toast';
import { Camera } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { ImageViewerController } from 'ionic-img-viewer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MapPage,
    GalleryPage,
    NavigationDetailsPage,
    ChatPage,
    ChatListPage,
    EmojiPickerComponent,
    RelativeTime
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MapPage,
    GalleryPage,
    NavigationDetailsPage,
    ChatPage,
    ChatListPage,
    EmojiPickerComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LaunchNavigator,
    Geolocation,
    EmojiProvider,
    ChatService,
    SQLite,
    DatabaseProvider,
    BLE,
    Camera,
    Toast,
    PhotoViewer,
    ImageViewerController,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
