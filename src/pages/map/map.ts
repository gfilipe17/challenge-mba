import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import leaflet from 'leaflet';

@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})

export class MapPage {
  @ViewChild('map') mapContainer: ElementRef;

  map       : any;
  data      : any;
  infoParam : any;
  points: Array<{
    lat           : number,
    long          : number,
    title         : string,
    image         : string,
    priceIncrease : boolean,
    priceMoney    : number,
    priceChange   : number,
    address       : string
  }>;

  constructor( public navCtrl: NavController, public navParams: NavParams, public storage: Storage ) {
    this.infoParam = this.navParams.get('coordinatesPoint');
    this.data = this.infoParam ? this.infoParam : this.getDefaultParam();
    storage.get('station-list').then((data) => {
      this.points = data;
    });
  }

  ionViewDidEnter() {
    this.loadMap();
    let that: any = this;
    setTimeout(function(){
      that.generateMarkers();
      that.getMyPosition();
    }, 500);
  }

  getDefaultParam () {
    return {
      lat: -15.505,
      long: -45.09,
      title: "Stations MAP",
      myLocation: true
    }
  }

  //create and loading map
  loadMap() {
    let {lat, long} = this.data;
    this.map = leaflet.map("map").setView([lat, long], 16);
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(this.map);
  }

  //get position actual of user
  getMyPosition() {
    this.map.locate({
      setView: this.data.myLocation,
      maxZoom: 25
    }).on('locationfound', (e) => {
      let userIcon: any = leaflet.icon({
        iconUrl: './assets/imgs/icon-user.png',
        iconSize: [50, 50]
      });
      leaflet.marker([e.latitude, e.longitude], {icon: userIcon}).addTo(this.map);
    }).on('locationerror', (err) => {
      alert(err.message);
    });
  }

  //create markers for the stations
  generateMarkers() {
    let markerGroup: any = leaflet.featureGroup();
    let pinIcon: any = leaflet.icon({
      iconUrl: './assets/imgs/pin-map-station.png',
      iconSize: [50, 50]
    });
    this.points.map((point) => {
      let urlImage: string = './assets/imgs/' + point.image;
      let marker: any = leaflet
        .marker([point.lat, point.long], {icon: pinIcon})
        .bindPopup("<img src='" + urlImage + "' width='42' height='42'><br><b>" + point.title + "</b><br>R$ " + point.priceMoney)
        .openPopup();
      markerGroup.addLayer(marker)
    });
    this.map.addLayer(markerGroup);
  }
}
