import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MapPage } from '../map/map';
import { Storage } from '@ionic/storage';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
import { Toast } from '@ionic-native/toast';
import { Geolocation } from '@ionic-native/geolocation';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  cards: Array<{
    lat           : number,
    long          : number,
    title         : string,
    image         : string,
    priceIncrease : boolean,
    priceMoney    : number,
    priceChange   : number,
    address       : string
  }>;

  constructor(
    public navCtrl: NavController,
    public storage: Storage,
    public launchNavigator: LaunchNavigator,
    public geolocation: Geolocation,
    public toast: Toast) {
      storage.get('station-list').then((data) => {
        this.cards = data;
      });
  }

  ionViewDidLoad(){
  }

  //open location in google maps
  onClickNavigate(station) {
    this.geolocation.getCurrentPosition().then((resp) => {
      let options: LaunchNavigatorOptions = {
        start: [resp.coords.latitude, resp.coords.longitude],
        app: this.launchNavigator.APP.GOOGLE_MAPS
      };
      this.launchNavigator.navigate([station.lat, station.long], options).then(
        success => console.log('Launched navigator'),
        error => console.log('Error launching navigator', error)
      );
    }).catch((error) => {
      this.toast.show(`Error - GPS`, '5000', 'top').subscribe(
        toast => {
          console.log(toast);
        }
      );

      console.log('Error getting location', error);
    });
  }

  //open point of station in map
  onClickPoint(card) {
    this.navCtrl.push(MapPage, { 'coordinatesPoint': card});
  }

}
