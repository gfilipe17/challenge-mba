import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChatService } from '../../providers/chat-service'

import { ChatPage } from '../chat/chat';

@Component({
  selector: 'chat-list',
  templateUrl: 'chat-list.html'
})
export class ChatListPage {

  toUser : {toUserId: string, toUserName: string, toUserAvatar: string};
  chatList: any[];
  constructor(public navCtrl: NavController, public chatService: ChatService) {

  }

  ionViewDidEnter() {
    this.getAllMessages();
  }

  //get all list messages
  getAllMessages() {
    this.chatService.getAll()
      .then((result: any[]) => {
      console.log("get all", result)
        this.chatList = result;
      });
  }

  //open chat by station
  openChat(station) {
    this.toUser = {
      toUserId    : station.id.toString(),
      toUserName  :station.name,
      toUserAvatar: station.image
    };
    this.navCtrl.push(ChatPage, this.toUser);
  }

}
