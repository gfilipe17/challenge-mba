import {Injectable} from '@angular/core';
import {Events} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {SQLiteObject} from '@ionic-native/sqlite';
import {DatabaseProvider} from '../providers/database/database';

export class ChatMessage {
  messageId: string;
  idChat?: string;
  userId: string;
  userName: string;
  userAvatar: string;
  toUserId: string;
  time: number | string;
  message: string;
  status: string;
}

export class UserInfo {
  id: string;
  name?: string;
  avatar?: string;
}

export const userAvatar = 'user-perfil.jpeg';

@Injectable()
export class ChatService {

  constructor(private events: Events, public storage: Storage, private dbProvider: DatabaseProvider) {
  }
  /**
 * Get data messages by id to station
 */
  public get(id: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from CHAT where id = ?';
        let data = [id];

        return db.executeSql(sql, data)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let item = data.rows.item(0);
              return item;
            }
            return null;
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }
  /**
   * Get all chats
   */
  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM CHAT';
        return db.executeSql(sql, [])
          .then((data: any) => {
            if (data.rows.length > 0) {
              let result: any[] = [];
              for (let i = 0; i < data.rows.length; i++) {
                let chat = data.rows.item(i);
                result.push(chat);
              }
              return result;
            } else {
              return [];
            }
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }
  /**
   * Insert message by station
   */
  public insert(msg: ChatMessage) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'insert into MESSAGES (messageId, idChat, userId, userName, userAvatar, toUserId, time, message, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)';
        let data = [msg.messageId, msg.idChat, msg.userId, msg.userName, msg.userAvatar, msg.toUserId, msg.time, msg.message, msg.status];

        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  /**
   * Create mock message to return information by station
   */
  mockNewMsg(msg, toUser) {
    let that = this;
    this.getUserInfo().then((user) => {
      const mockMsg: ChatMessage = {
        messageId: Date.now().toString(),
        idChat: toUser.id,
        userId: toUser.id,
        userName: toUser.name,
        userAvatar: toUser.avatar,
        toUserId: user.id,
        time: Date.now(),
        message: msg.message,
        status: 'success'
      };
      setTimeout(() => {
        this.events.publish('chat:received', mockMsg, Date.now());
        that.insert(mockMsg);
        console.log("mandou mock ", mockMsg);
      }, Math.random() * 1800)
    });
  }
  /**
   * Get list messages by station
   */
  getMsgList(id:string) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'select * from MESSAGES where idChat = ?';
        let data = [id];
        return db.executeSql(sql, data)
          .then((data: any) => {
            console.info(data);
            if (data.rows.length > 0) {
              let result: any[] = [];
              for(let i = 0; i < data.rows.length; i++){
                result.push(data.rows.item(i))
              }
              return result;
            }
            return null;
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  /**
   * Send message for da
   */
  sendMsg(msg: ChatMessage, toUser) {
    return new Promise(resolve => setTimeout(() => resolve(msg), Math.random() * 1000))
      .then(() => this.mockNewMsg(msg, toUser));
  }

  getUserInfo(): Promise<UserInfo> {
    const userInfo: UserInfo = {
      id: "164853678",
      name: 'Gabriel',
      avatar: userAvatar
    };
    return new Promise(resolve => resolve(userInfo));
  }

}
