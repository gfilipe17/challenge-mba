import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

@Injectable()
export class DatabaseProvider {

  constructor(private sqlite: SQLite) { }

  /**
   * Create database
   */
  public getDB() {
    return this.sqlite.create({
      name: 'products.db',
      location: 'default'
    });
  }

  /**
   * Create init structure database
   */
  public createDatabase() {
    return this.getDB()
      .then((db: SQLiteObject) => {

        // Create tables
        this.createTables(db);

        // Insert data
        this.insertDefaultItems(db);

      })
      .catch(e => console.log(e));
  }

  /**
   * Create tables in database
   * @param db
   */
  private createTables(db: SQLiteObject) {
    // Create tables
    db.sqlBatch([
      ['CREATE TABLE IF NOT EXISTS CHAT (id integer PRIMARY KEY AUTOINCREMENT, name text, image text)'],
      ['CREATE TABLE IF NOT EXISTS MESSAGES (id integer PRIMARY KEY AUTOINCREMENT, messageId text, idChat text, userId text, userName text, userAvatar text, toUserId text, time integer, message text, status text)']
    ])
      .then(() => console.log('Create tables'))
      .catch(e => console.error('Error create tables', e));
  }

  /**
   * Include data
   * @param db
   */
  private insertDefaultItems(db: SQLiteObject) {
    db.executeSql('select COUNT(id) as qtd from CHAT', {})
      .then((data: any) => {
        //if not data
        if (data.rows.item(0).qtd == 0) {

          // create tables
          db.sqlBatch([
            ['insert into CHAT (name, image) values (?, ?)', ['BR', 'br.jpg']],
            ['insert into CHAT (name, image) values (?, ?)', ['O MELHOR', 'o-melhor.jpg']],
            ['insert into MESSAGES (messageId, idChat, userId, userName, userAvatar, toUserId, time, message, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ["1","1","1", 'BR', 'br.jpg', "164853678", 1529467138561, 'Promoção para clientes..','success']],
            ['insert into MESSAGES (messageId, idChat, userId, userName, userAvatar, toUserId, time, message, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ["3","2","2", 'O MELHOR', 'o-melhor.jpg', "164853678", 1529468042751, 'Código para desconto:','success']],
            ['insert into MESSAGES (messageId, idChat, userId, userName, userAvatar, toUserId, time, message, status) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', ["4","2","2", 'O MELHOR', 'o-melhor.jpg', "164853678", 1529468042751, 'PRO123','success']],
          ])
            .then(() => console.log('Include data SUCCESS'))
            .catch(e => console.error('Include data ERROR', e));

        }
      })
      .catch(e => console.error('Error ', e));
  }
}
